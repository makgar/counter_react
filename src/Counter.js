import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';

class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
    this.handleDecrease = this.handleDecrease.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleIncrease = this.handleIncrease.bind(this);
  }

  handleDecrease() {
    this.setState((prevState, { count }) => ({
      count: prevState.count - 1
    }));
  }
  handleReset() {
    this.setState(state => ({
      count: 0
    }));
  }
  handleIncrease() {
    this.setState((prevState, { count }) => ({
      count: prevState.count + 1
    }));
  }

  render() {
    return (
      <div className="Counter">
        <h1>Counter</h1>
        <p>{this.state.count}</p>
        <button onClick={() => this.handleDecrease()}>Decrease</button>
        <button onClick={() => this.handleReset()}>Reset</button>
        <button onClick={() => this.handleIncrease()}>Increase</button>
      </div>
    );
  }
}

export default Counter;
